﻿using Manifest_Diagnostics_Tool;
using Manifest_Diagnostics_Tool.Controllers;
using Manifest_Diagnostics_Tool.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SideBySideTests
{
    [TestClass]
    public class APITests
    {
        readonly ManifestDiagnosticAPI _api = new ManifestDiagnosticAPI(new XMLSettingsFile(), new FileDetailList(new MTController()), new FileDetailList(new MTController()), new MTController());
        readonly Mock<IFileDetailList> fakeLibs = new Mock<IFileDetailList>();
        readonly Mock<IFileDetailList> fakeExes = new Mock<IFileDetailList>();

        public APITests()
        {
            fakeLibs.Setup(c => c.CreateFileDetails(It.IsAny<List<string>>(), It.IsAny<bool>()));
            fakeExes.Setup(c => c.CreateFileDetails(It.IsAny<List<string>>(), It.IsAny<bool>()));

            _api.Libraries = fakeLibs.Object;
            _api.Executables = fakeExes.Object;
        }

        [TestMethod]
        public void MT_not_installed_throws()
        {
            // arrange
            var fakeMT = new Mock<MTController>();
            fakeMT.Object.MTLocation = "";

            var _api = new ManifestDiagnosticAPI(new XMLSettingsFile(), new FileDetailList(fakeMT.Object), new FileDetailList(fakeMT.Object), fakeMT.Object)
            {
                ExtractNewManifests = true
            };

            // act
            var result = _api.GetLibrariesAsync();

            // assert
            Assert.IsTrue(result.Exception.InnerException.Message.Contains("mt.exe not found"));
        }

        [TestMethod]
        public void Invalid_path_throws()
        {
            // arrange
            var fakeMT = new Mock<MTController>();
            fakeMT.Setup(f => f.CheckForMTInstalled(It.IsAny<string>())).Returns(true);

            var _api = new ManifestDiagnosticAPI(new XMLSettingsFile(), new FileDetailList(fakeMT.Object), new FileDetailList(fakeMT.Object), fakeMT.Object)
            {
                SDKPath = "",
                ExtractNewManifests = true
            };

            // act
            var result = _api.GetLibrariesAsync();

            // assert
            Assert.AreNotEqual(result.Exception.InnerException.Message, "");
        }

        [TestMethod]
        public void ReadSettings_sets_path_and_list()
        {
            // arrange
            var fakeSettings = new Mock<ISettingsFile>();
            fakeSettings.Setup(f => f.ApplicationPaths).Returns(new List<string>() { "a", "b" });
            fakeSettings.Setup(f => f.SDKPath).Returns("x");

            var _api = new ManifestDiagnosticAPI(fakeSettings.Object, new FileDetailList(new MTController()), new FileDetailList(new MTController()), new MTController());

            // act
            _api.ReadSettingsXML();

            // assert
            Assert.AreEqual(_api.SDKPath, "x");
        }
    }
}
