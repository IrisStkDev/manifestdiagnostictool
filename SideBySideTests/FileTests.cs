﻿using System;
using Manifest_Diagnostics_Tool;
using Manifest_Diagnostics_Tool.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SideBySideTests
{
    [TestClass]
    public class FileTests
    {
        readonly IFileDetail _fileDetails;

        public FileTests()
        {
            var sourceManifest = new SourceManifest(new MTController(), "")
            {
                Architecture = "",
                FilePath = "",
                Name = "",
                PublicKey = "",
                Version = Version.Parse("1.0"),
                Location = ""
            };

            var managedManifest = new ManagedManifest(new MTController(), "")
            {
                Architecture = "",
                FilePath = "",
                Name = "",
                PublicKey = "",
                Version = Version.Parse("1.0"),
                Location = ""
            };

            _fileDetails = new FileDetail(sourceManifest, managedManifest);

            _fileDetails.FileVersion = Version.Parse("1.0");
        }

        [TestMethod]
        public void FileDetailsValid()
        {
            // Assert
            Assert.IsTrue(_fileDetails.Valid);
        }

        [TestMethod]
        public void FileDetailsInvalid()
        {
            // Arrange
            _fileDetails.FileVersion = Version.Parse("1.1");

            // Assert
            Assert.IsFalse(_fileDetails.Valid);
        }

    }
}
