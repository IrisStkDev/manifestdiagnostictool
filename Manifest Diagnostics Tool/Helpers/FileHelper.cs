﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manifest_Diagnostics_Tool.Helpers
{
    public static class FileHelper
    {
        public static IEnumerable<string> GetFilenames(string path, string fileExtension)
        {
            IEnumerable<string> fileNames = Directory.EnumerateFiles(path, $"*.{fileExtension}", SearchOption.AllDirectories);

            // don't include common libraries as these aren't relevant
            fileNames = fileNames.Where(c => !c.Contains("DevExpress."));
            fileNames = fileNames.Where(c => !c.Contains("Codejock."));
            fileNames = fileNames.Where(c => !c.Contains("System."));

            return fileNames;
        }
    }
}
