﻿using Manifest_Diagnostics_Tool.Controllers;
using System.Diagnostics;
using System.IO;

namespace Manifest_Diagnostics_Tool.Models
{
    public class ManagedManifest : Manifest
    {
        public ManagedManifest(IMTController mTController, string path) : base(mTController, path)
        {
            FilePath = path;
            Location = FilePath + "_managed.manifest";
        }

        public override void Extract()
        {
            using (Process p = new Process())
            {
                p.StartInfo = new ProcessStartInfo(_mTController.MTFile, $" -managedassemblyname:\"{FilePath}\" -out:\"{ Location}\"" + " -category")
                {
                    UseShellExecute = false,
                    CreateNoWindow = true
                    //RedirectStandardOutput = true,
                    //RedirectStandardError = true
                };
                p.Start();
                p.WaitForExit(10000);
            }

            _creationTime = File.GetCreationTime(Location);
        }
    }
}
