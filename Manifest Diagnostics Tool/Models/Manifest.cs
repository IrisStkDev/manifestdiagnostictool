﻿using Manifest_Diagnostics_Tool.Controllers;
using System;
using System.IO;
using System.Xml;

namespace Manifest_Diagnostics_Tool.Models
{
    public abstract class Manifest : IManifest
    {
        public string Location { get; set; }
        public string PublicKey { get; set; }
        public string Architecture { get; set; }
        public string Name { get; set; }
        public Version Version { get; set; }
        public string FilePath { get; set; }
        public bool FileExists { get { return File.Exists(Location); } }
        public DateTime ExtractionTime { get { return _creationTime; } }
        protected DateTime _creationTime { get; set; }

        protected readonly IMTController _mTController;

        public Manifest(IMTController mTController, string path = null)
        {
            FilePath = path;
            _mTController = mTController;
        }

        public abstract void Extract();

        public virtual void Read()
        {
            XmlDocument doc = new XmlDocument();

            try
            {
                doc.Load(Location);
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("urn", "schemas - microsoft - com:asm.v1");
                XmlNode assemblyIdentityNode = doc.DocumentElement.FirstChild;
                if (assemblyIdentityNode != null)
                {
                    Name = assemblyIdentityNode.Attributes["name"].Value;
                    Version = new Version(assemblyIdentityNode.Attributes["version"].Value);

                    if (assemblyIdentityNode.Attributes["publicKeyToken"] != null)
                    {
                        PublicKey = assemblyIdentityNode.Attributes["publicKeyToken"].Value;
                    }

                    if (assemblyIdentityNode.Attributes["processorArchitecture"] != null)
                    {
                        Architecture = assemblyIdentityNode.Attributes["processorArchitecture"].Value;
                    }
                }
            }
            catch
            {
                //doc = null;
            }
            finally
            {
                //doc = null;
            }
        }

        public virtual void Delete()
        {
            if (FileExists) { File.Delete(Location); }
        }
    }
}
