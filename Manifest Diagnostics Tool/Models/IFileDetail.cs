﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manifest_Diagnostics_Tool.Models
{
    public interface IFileDetail
    {
        string FileLocation { get; set; }
        Version FileVersion { get; set; }
        string Notes { get; set; }

        bool Valid { get; }
        Version LibraryVersion { get; }
        string PublicKey { get; }
        string Architecture { get; }
        string ManagedPublicKey { get; }
        string ManagedArchitecture { get; }
        string FileName { get; }
        
        void Extract();
        void Read();
    }
}
