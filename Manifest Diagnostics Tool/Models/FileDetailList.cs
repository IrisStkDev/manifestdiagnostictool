﻿using Manifest_Diagnostics_Tool.Controllers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Manifest_Diagnostics_Tool.Models
{
    public class FileDetailList : IFileDetailList
    {
        private List<IFileDetail> Files;
        private readonly IMTController _mTController;

        private List<string> FileNames
        {
            get
            {
                return Files.Select(c => c.FileName).ToList();
            }
        }

        public bool Valid
        {
            get
            {
                return Files.Where(c => c.Valid == false).Count() == 0;
            }
        }

        public FileDetailList(IMTController mTController)
        {
            Files = new List<IFileDetail>();
            _mTController = mTController;
        }

        public List<IFileDetail> GetFiles()
        {
            return Files;
        }

        public async Task CreateFileDetails(IEnumerable<string> fileList, bool extractNewManifests)
        {
            // ideally run this asynchronously, but it doesn't trigger using Task.Run for some reason
            //await Task.Run(() =>
            //{
                fileList.ToList().ForEach(delegate (string filePath)
                {
                    PopulateFiles(filePath, extractNewManifests);
                });
            //});
        }

        public void Clear()
        {
            Files.Clear();
        }

        private void PopulateFiles(string path, bool extractNewManifests)
        {
            var fDetails = new FileDetail(path, _mTController);

            if (_mTController.CheckForMTInstalled(null) && extractNewManifests)
            {
                fDetails.Extract();
            }

            fDetails.Read();

            Files.Add(fDetails);
        }
    }
}
