﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manifest_Diagnostics_Tool.Models
{
    public interface IFileDetailList
    {
        bool Valid { get; }

        List<IFileDetail> GetFiles();
        Task CreateFileDetails(IEnumerable<string> fileList, bool extractNewManifests);
        void Clear();
    }
}
