﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manifest_Diagnostics_Tool.Models
{
    interface IManifest
    {
        string FilePath { get; set; }
        string Location { get; }
        string PublicKey { get; set; }
        string Architecture { get; set; }
        string Name { get; set; }
        Version Version { get; set; }
        bool FileExists { get; }

        void Extract();

        void Delete();

        void Read();

    }
}
