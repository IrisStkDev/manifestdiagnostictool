﻿using Manifest_Diagnostics_Tool.Controllers;
using System.Diagnostics;
using System.IO;

namespace Manifest_Diagnostics_Tool.Models
{
    public class SourceManifest : Manifest
    {
        public SourceManifest(IMTController mTController, string path) : base(mTController, path)
        {
            FilePath = path;
            Location = FilePath + ".manifest";
        }

        public override void Extract()
        {
            ExtractSlot(1);

            ExtractSlot(2);
        }

        private void ExtractSlot(int slotNumber)
        {
            using (Process p = new Process())
            {
                p.StartInfo = new ProcessStartInfo(_mTController.MTFile, $"-inputresource:\"{FilePath}\";#{slotNumber} -out:\"{Location}\"")
                {
                    UseShellExecute = false,
                    CreateNoWindow = true
                    //RedirectStandardOutput = true,
                    //RedirectStandardError = true
                };
                p.Start();
                p.WaitForExit(5000);
            }

            _creationTime = File.GetCreationTime(Location);
        }
    }
}
