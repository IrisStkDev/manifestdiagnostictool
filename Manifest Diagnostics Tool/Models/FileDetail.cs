﻿using Manifest_Diagnostics_Tool.Controllers;
using System;
using System.Diagnostics;
using System.IO;

namespace Manifest_Diagnostics_Tool.Models
{
    public class FileDetail : IFileDetail
    {
        public string FileLocation {  get; set;}
        public Version FileVersion { get; set; }
        public string Notes { get; set; }

        public bool Valid { get { return IsValid(); } }
        public Version LibraryVersion { get { return ManagedManifest.Version ?? SourceManifest.Version; } }
        public string PublicKey { get { return SourceManifest.PublicKey; } }
        public string Architecture { get { return SourceManifest.Architecture; } }
        public string ManagedPublicKey { get {return ManagedManifest.PublicKey; } }
        public string ManagedArchitecture { get{ return ManagedManifest.Architecture; } }

        private readonly IManifest SourceManifest;
        private readonly IManifest ManagedManifest;

        public string FileName { get { return Path.GetFileName(FileLocation); } }
        public string Extension { get { return Path.GetExtension(FileLocation); } }

        private readonly IMTController _mTController;

        public FileDetail(string name, IMTController mTController)
        {
            FileLocation = name;

            var versionInfo = FileVersionInfo.GetVersionInfo(FileLocation);
            FileVersion = new Version(versionInfo.FileMajorPart, versionInfo.FileMinorPart, versionInfo.FileBuildPart, versionInfo.FilePrivatePart);

            SourceManifest = new SourceManifest(mTController, FileLocation);
            ManagedManifest = new ManagedManifest(mTController, FileLocation);

            _mTController = mTController;
        }

        public FileDetail(SourceManifest sourceManifest, ManagedManifest managedManifest)
        {
            SourceManifest = sourceManifest;
            ManagedManifest = managedManifest;
        }

        public void Extract()
        {
            SourceManifest.Extract();
            ManagedManifest.Extract();
        }

        public void Read()
        {
            SourceManifest.Read();
            ManagedManifest.Read();
        }

        private void Delete()
        {
            SourceManifest.Delete();
            ManagedManifest.Delete();
        }

        private enum ManifestType
        {
            Both = 0,
            ApplicationManifest = 1,
            ManagedAssebly = 2, 
            None = 4
        }

        private ManifestType Type
        {
            get
            {
                if (SourceManifest.FileExists && ManagedManifest.FileExists) return ManifestType.Both;

                if (SourceManifest.FileExists) return ManifestType.ApplicationManifest;

                if (ManagedManifest.FileExists) return ManifestType.ManagedAssebly;

                return ManifestType.None;
            }
        }

        private bool IsValid()
        {
            Notes = String.Empty;

            if (LibraryVersion != FileVersion && !(LibraryVersion == null || String.IsNullOrWhiteSpace(LibraryVersion.ToString())))
            {
                Notes = "Failed: File Version and Library Versions do not match, this is likely caused by an incorrect embedded manifest";
                return false;
            }

            if ((SourceManifest.PublicKey != null) && (SourceManifest.PublicKey != ManagedManifest.PublicKey))
            {
                Notes = "Failed: Public keys don't match, this is likely caused by an incorrect embedded manifest";
                return false;
            }

            if ((SourceManifest.Architecture != null) && (SourceManifest.Architecture != ManagedManifest.Architecture))
            {
                Notes = "Failed: Architecture types don't match, this is likely caused by an incorrect setting in the compilation machine (should be x86)";
                return false;
            }

            Notes = "Passed";
            return true;
        }
    }
}
