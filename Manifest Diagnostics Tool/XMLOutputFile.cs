﻿using Manifest_Diagnostics_Tool.Models;
using System.Diagnostics;
using System.IO;
using System.Xml;

namespace Manifest_Diagnostics_Tool
{
    public class XMLOutputFile
    {
        private const string xmlFilePath = "SBSOutput.xml";

        public void Open()
        {
            if (!File.Exists(xmlFilePath))
            {
                throw new FileNotFoundException();
            }

            Process.Start(xmlFilePath);
        }

        public void Read()
        {

        }

        public void Save(IFileDetailList libraries, IFileDetailList executables)
        {
            var xmlOutputFile = new XmlDocument();
            ClearXMLDocument(xmlOutputFile);

            xmlOutputFile.DocumentElement.AppendChild(GetXmlElement(xmlOutputFile, "OverallValidity", (libraries.Valid && executables.Valid).ToString()));

            foreach (FileDetail library in libraries.GetFiles())
            {
                XmlElement element = GetXmlElement(xmlOutputFile, "dll", library.FileName);
                XmlElement validity = GetXmlElement(xmlOutputFile, "validity", library.Valid.ToString());
                XmlElement note = GetXmlElement(xmlOutputFile, "note", library.Notes);

                element.AppendChild(validity);
                element.AppendChild(note);

                xmlOutputFile.DocumentElement.AppendChild(element);
            }

            foreach (FileDetail executable in executables.GetFiles())
            {
                XmlElement element = GetXmlElement(xmlOutputFile, "exe", executable.FileName);
                XmlElement validity = GetXmlElement(xmlOutputFile, "validity", executable.Valid.ToString());
                XmlElement note = GetXmlElement(xmlOutputFile, "note", executable.Notes);

                element.AppendChild(validity);
                element.AppendChild(note);

                xmlOutputFile.DocumentElement.AppendChild(element);
            }

            xmlOutputFile.Save(xmlFilePath);
        }

        private void ClearXMLDocument(XmlDocument xmlOutputFile)
        {
            if (!File.Exists(xmlFilePath)) CreateNewXMLDocument();

            xmlOutputFile.Load(xmlFilePath);

            if (xmlOutputFile.DocumentElement == null) CreateNewXMLDocument();

            foreach (XmlNode node in xmlOutputFile.SelectNodes("//OverallValidity"))
            {
                node.ParentNode.RemoveChild(node);
            }

            foreach (XmlNode node in xmlOutputFile.SelectNodes("//dll"))
            {
                node.ParentNode.RemoveChild(node);
            }

            foreach (XmlNode node in xmlOutputFile.SelectNodes("//exe"))
            {
                node.ParentNode.RemoveChild(node);
            }
        }

        private void CreateNewXMLDocument()
        {
            var xmlSettingsFile = new XmlDocument();
            xmlSettingsFile.AppendChild(xmlSettingsFile.CreateElement("xml"));
            xmlSettingsFile.Save(xmlFilePath);
        }

        private XmlElement GetXmlElement(XmlDocument xmlOutputFile, string name, string value)
        {
            XmlElement element;
            XmlAttribute attribute;

            element = xmlOutputFile.CreateElement(name);
            attribute = xmlOutputFile.CreateAttribute("value");
            attribute.Value = value;
            element.Attributes.Append(attribute);

            return element;
        }
    }
}
