﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace Manifest_Diagnostics_Tool
{
    public class XMLSettingsFile : ISettingsFile
    {
        private string _xmlFilePath = "SBSSettings.xml";
        private List<string> _applicationPaths;
        private string _sdkPath;

        private XmlDocument _xmlSettingsFile = new XmlDocument();

        public string FilePath
        {
            get { return _xmlFilePath; }
        }

        public List<string> ApplicationPaths
        {
            get { return _applicationPaths.ToList(); }
        }

        public string SDKPath
        {
            get { return _sdkPath; }
        }

        public XMLSettingsFile(string xmlFilePath = "SBSSettings.xml")
        {
            _xmlFilePath = xmlFilePath;
            _xmlSettingsFile = new XmlDocument();

            if (File.Exists(_xmlFilePath))
            {
                _xmlSettingsFile.Load(_xmlFilePath);
                if (_xmlSettingsFile.DocumentElement == null) CreateNewXMLDocument();
            }
            else
            {
                CreateNewXMLDocument();
            }
        }

        public void Read()
        {
            _applicationPaths = new List<string>();

            if (File.Exists(_xmlFilePath))
            {
                _xmlSettingsFile.Load(_xmlFilePath);

                foreach (XmlNode node in _xmlSettingsFile.SelectNodes("//Path"))
                {
                    if (node.Attributes.Count > 0)
                    {
                        _applicationPaths.Add(node.Attributes.Item(0).Value);
                    }
                }

                foreach (XmlNode sdkNode in _xmlSettingsFile.SelectNodes("//SDK"))
                {
                    if (sdkNode.Attributes.Count > 0)
                    {
                        _sdkPath = sdkNode.Attributes.Item(0).Value;
                    }
                }
            }
        }

        public void Save(IEnumerable<string> ApplicationPaths, string SDKPath)
        {
            ClearXMLFile();

            if (_xmlSettingsFile.DocumentElement == null)
            {
                _xmlSettingsFile.AppendChild(_xmlSettingsFile.CreateElement("xml"));
            }

            XmlElement element;
            XmlAttribute attribute;

            foreach (string path in ApplicationPaths)
            {
                element = _xmlSettingsFile.CreateElement("Path");
                attribute = _xmlSettingsFile.CreateAttribute("value");
                attribute.Value = path;
                element.Attributes.Append(attribute);
                _xmlSettingsFile.DocumentElement.AppendChild(element);
            }

            element = _xmlSettingsFile.CreateElement("SDK");
            attribute = _xmlSettingsFile.CreateAttribute("value");
            attribute.Value = SDKPath;
            element.Attributes.Append(attribute);
            _xmlSettingsFile.DocumentElement.AppendChild(element);
            _xmlSettingsFile.Save(_xmlFilePath);
        }

        private void CreateNewXMLDocument()
        {
            _xmlSettingsFile = new XmlDocument();
            _xmlSettingsFile.AppendChild(_xmlSettingsFile.CreateElement("xml"));
            _xmlSettingsFile.Save(_xmlFilePath);
        }

        private void ClearXMLFile()
        {
            foreach (XmlNode node in _xmlSettingsFile.SelectNodes("//Path"))
            {
                node.ParentNode.RemoveChild(node);
            }

            foreach (XmlNode node in _xmlSettingsFile.SelectNodes("//SDK"))
            {
                node.ParentNode.RemoveChild(node);
            }
        }
    }
}
