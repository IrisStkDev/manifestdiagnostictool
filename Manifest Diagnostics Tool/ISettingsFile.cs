﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manifest_Diagnostics_Tool
{
    public interface ISettingsFile
    {
        string FilePath { get; }
        List<string> ApplicationPaths { get; }
        string SDKPath { get; }

        void Read();
        void Save(IEnumerable<string> ApplicationPaths, string SDKPath);
    }
}
