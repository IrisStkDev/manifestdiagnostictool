﻿using Manifest_Diagnostics_Tool.Controllers;
using Manifest_Diagnostics_Tool.Helpers;
using Manifest_Diagnostics_Tool.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Manifest_Diagnostics_Tool
{
    public class ManifestDiagnosticAPI
    {
        public string SDKPath { get; set; }
        public List<string> ApplicationPaths { get; set; }
        public IFileDetailList Libraries { get; set; }
        public IFileDetailList Executables { get; set; }
        public bool ExtractNewManifests = true;

        private readonly ISettingsFile _xmlSettingsFile;

        private readonly IMTController _mTController;

        public ManifestDiagnosticAPI(ISettingsFile xmlSettingsFile, IFileDetailList libraries, IFileDetailList executables, IMTController mTController)
        {
            ApplicationPaths = new List<string> { };

            _xmlSettingsFile = xmlSettingsFile;
            _mTController = mTController;
            ReadSettingsXML();

            Libraries = libraries;
            Executables = executables;
        }

        public void ReadSettingsXML()
        {
            _xmlSettingsFile.Read();

            ApplicationPaths = _xmlSettingsFile.ApplicationPaths;
            SDKPath = _xmlSettingsFile.SDKPath;
        }

        public async Task GetLibrariesAsync()
        {
            //_mTController.MTLocation = SDKPath;
            ClearAllFiles();

            if (!_mTController.CheckForMTInstalled(null) && ExtractNewManifests)
            {
                throw new Exception($"mt.exe not found at the specified location: {_mTController.MTLocation}");
            }

            if(ApplicationPaths.Count == 0)
            {
                throw new Exception($"No application paths specified, check {_xmlSettingsFile.FilePath} contents.");
            }
            
            foreach (string path in ApplicationPaths)
            {
                if (Directory.Exists(path))
                {
                    await Libraries.CreateFileDetails(FileHelper.GetFilenames(path, "dll"), ExtractNewManifests);
                    await Executables.CreateFileDetails(FileHelper.GetFilenames(path, "exe"), ExtractNewManifests);
                }
                else
                {
                    throw new FileNotFoundException($"The selected folder {path} does not exist." + Environment.NewLine + Environment.NewLine + "Please change or remove that item from the list.");
                }
            }
        }

        private void ClearAllFiles()
        {
            Libraries.Clear();
            Executables.Clear();
        }

        public void AddNewFilePath(string path)
        {
            ApplicationPaths.Add(path);
            _xmlSettingsFile.Save(ApplicationPaths, SDKPath);
        }

        public void SaveXMLSettings()
        {
            _xmlSettingsFile.Save(ApplicationPaths, SDKPath);
        }

        public void DeletePath(string path)
        {
            ApplicationPaths.Remove(path);
        }

        public void OutputToXML()
        {
            XMLOutputFile outputFile = new XMLOutputFile();
            outputFile.Save(Libraries, Executables);
        }

        public void OpenOutputXML()
        {
            XMLOutputFile outputFile = new XMLOutputFile();
            outputFile.Open();
        }
    }
}
