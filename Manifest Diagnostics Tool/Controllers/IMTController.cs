﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manifest_Diagnostics_Tool.Controllers
{
    public interface IMTController
    {
        string MTFile {get;}

        string MTLocation { get; set; }

        bool CheckForMTInstalled(string path);
    }
}
