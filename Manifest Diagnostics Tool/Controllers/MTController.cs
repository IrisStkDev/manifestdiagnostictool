﻿using Manifest_Diagnostics_Tool.Controllers;
using System.Collections.Generic;
using System.IO;

namespace Manifest_Diagnostics_Tool
{
    public class MTController : IMTController
    {
        // default locations for mt.exe, since a lot of users won't know where theirs is
        private readonly List<string> _defaultPaths = new List<string>()
        {
            @"C:\Program Files (x86)\Windows Kits\10\bin\x86",
            @"Tools"
        };

        private readonly string FileName = @"mt.exe";

        private string _path;

        public MTController()
        { }

        public string MTLocation
        {
            get { return @"Tools"; }
            set { _path = value; }
        }

        public string MTFile
        {
            get
            {
                return MTLocation + @"\mt.exe";
            }
        }

        public virtual bool CheckForMTInstalled(string path)
        {
            string fileName = MTFile;
            return File.Exists(fileName) ? true : false;
        }

        private string FirstMTexeFound()
        {
            foreach (var path in _defaultPaths)
            {
                if (CheckForMTInstalled(path)) return path;
            }

            return null;
        }
    }
}
