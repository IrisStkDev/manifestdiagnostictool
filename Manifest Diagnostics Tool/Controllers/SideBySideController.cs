﻿using Manifest_Diagnostics_Tool.Models;
using Manifest_Diagnostics_Tool.Views;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Manifest_Diagnostics_Tool
{
    public class SideBySideController
    {
        private readonly MainWindow _form;
        private readonly ManifestDiagnosticAPI _API;

        public IFileDetailList Libraries { get { return _API.Libraries; } }
        public IFileDetailList Executables { get { return _API.Executables; } }

        public bool ExtractNewManifests = true;

        public SideBySideController(Form view, XMLSettingsFile xmlSettingsFile, ManifestDiagnosticAPI api)
        {
            _form = view as MainWindow;
            _form.AddListener(this);
            _API = api;

            RefreshPathList();
            _form.ShowDialog();
        }

        private void RefreshPathList()
        {
            _form.lstInstallPaths.Items.Clear();

            foreach (string path in _API.ApplicationPaths)
            {
                _form.lstInstallPaths.Items.Add(path);
            }

            if (_API.SDKPath != null)
            {
                _form.TxtWindowsSDKFolder.Text = _API.SDKPath;
            }
        }

        public void DeleteSelectedListItem()
        {
            if (_form.lstInstallPaths.SelectedItems.Count > 0)
            {
                _API.DeletePath(_form.lstInstallPaths.SelectedItem.ToString());
                RefreshPathList();
            }
        }

        public async Task<bool> GetLibrariesAsync()
        {
            SetFormRunning(true);

            try
            {
                await _API.GetLibrariesAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Manifest Diagnostics", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SetFormRunning(false);
            return true;
        }
        
        private void SetFormRunning(bool wait)
        {
            _form.SetFormRunning(wait);
        }

        public void AddNewFilePath()
        {
            FolderBrowserDialog file = new FolderBrowserDialog();
            if (file.ShowDialog() == DialogResult.OK)
            {
                _API.AddNewFilePath(file.SelectedPath);
                RefreshPathList();
            }
        }

        public void SetNewSDKPath()
        {
            FolderBrowserDialog file = new FolderBrowserDialog();
            if(file.ShowDialog() == DialogResult.OK)
            {
                _API.SDKPath = file.SelectedPath;
                _API.SaveXMLSettings();
            }
            RefreshPathList();
        }

        public void SetExtractNewManifests(bool extractNewManifests)
        {
            _API.ExtractNewManifests = extractNewManifests;
        }

        public void SaveResultsToXML()
        {
            _API.OutputToXML();

            var result = MessageBox.Show($"Export complete. {Environment.NewLine} Show results?", "Output", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                _API.OpenOutputXML();
            }
        }
    }
}