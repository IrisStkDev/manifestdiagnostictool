﻿namespace Manifest_Diagnostics_Tool.Views
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkUseExisting = new System.Windows.Forms.CheckBox();
            this.BtnAddPath = new System.Windows.Forms.Button();
            this.BtnDeletePath = new System.Windows.Forms.Button();
            this.lstInstallPaths = new System.Windows.Forms.ListBox();
            this.BtnDisplay = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.TxtWindowsSDKFolder = new System.Windows.Forms.TextBox();
            this.lblMtNotFound = new System.Windows.Forms.Label();
            this.BtnSearchMT = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // chkUseExisting
            // 
            this.chkUseExisting.AutoSize = true;
            this.chkUseExisting.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkUseExisting.Checked = true;
            this.chkUseExisting.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUseExisting.Location = new System.Drawing.Point(504, 245);
            this.chkUseExisting.Name = "chkUseExisting";
            this.chkUseExisting.Size = new System.Drawing.Size(132, 17);
            this.chkUseExisting.TabIndex = 14;
            this.chkUseExisting.Text = "Extract New Manifests";
            this.chkUseExisting.UseVisualStyleBackColor = true;
            // 
            // BtnAddPath
            // 
            this.BtnAddPath.Location = new System.Drawing.Point(642, 127);
            this.BtnAddPath.Name = "BtnAddPath";
            this.BtnAddPath.Size = new System.Drawing.Size(75, 23);
            this.BtnAddPath.TabIndex = 12;
            this.BtnAddPath.Text = "Add";
            this.BtnAddPath.UseVisualStyleBackColor = true;
            this.BtnAddPath.Click += new System.EventHandler(this.BtnAddPath_Click);
            // 
            // BtnDeletePath
            // 
            this.BtnDeletePath.Location = new System.Drawing.Point(642, 98);
            this.BtnDeletePath.Name = "BtnDeletePath";
            this.BtnDeletePath.Size = new System.Drawing.Size(75, 23);
            this.BtnDeletePath.TabIndex = 11;
            this.BtnDeletePath.Text = "Delete";
            this.BtnDeletePath.UseVisualStyleBackColor = true;
            this.BtnDeletePath.Click += new System.EventHandler(this.BtnDeletePath_Click);
            // 
            // lstInstallPaths
            // 
            this.lstInstallPaths.FormattingEnabled = true;
            this.lstInstallPaths.Location = new System.Drawing.Point(35, 98);
            this.lstInstallPaths.Name = "lstInstallPaths";
            this.lstInstallPaths.Size = new System.Drawing.Size(596, 108);
            this.lstInstallPaths.TabIndex = 10;
            // 
            // BtnDisplay
            // 
            this.BtnDisplay.Location = new System.Drawing.Point(642, 240);
            this.BtnDisplay.Name = "BtnDisplay";
            this.BtnDisplay.Size = new System.Drawing.Size(74, 25);
            this.BtnDisplay.TabIndex = 15;
            this.BtnDisplay.Text = "Go";
            this.BtnDisplay.UseVisualStyleBackColor = true;
            this.BtnDisplay.Click += new System.EventHandler(this.BtnDisplay_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // TxtWindowsSDKFolder
            // 
            this.TxtWindowsSDKFolder.Location = new System.Drawing.Point(35, 245);
            this.TxtWindowsSDKFolder.Name = "TxtWindowsSDKFolder";
            this.TxtWindowsSDKFolder.Size = new System.Drawing.Size(350, 20);
            this.TxtWindowsSDKFolder.TabIndex = 16;
            this.TxtWindowsSDKFolder.Visible = false;
            this.TxtWindowsSDKFolder.TextChanged += new System.EventHandler(this.TxtWindowsSDKFolder_TextChanged);
            // 
            // lblMtNotFound
            // 
            this.lblMtNotFound.AutoSize = true;
            this.lblMtNotFound.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMtNotFound.Location = new System.Drawing.Point(43, 268);
            this.lblMtNotFound.Name = "lblMtNotFound";
            this.lblMtNotFound.Size = new System.Drawing.Size(138, 13);
            this.lblMtNotFound.TabIndex = 17;
            this.lblMtNotFound.Text = "mt.exe not found at location";
            this.lblMtNotFound.Visible = false;
            // 
            // BtnSearchMT
            // 
            this.BtnSearchMT.Location = new System.Drawing.Point(354, 246);
            this.BtnSearchMT.Name = "BtnSearchMT";
            this.BtnSearchMT.Size = new System.Drawing.Size(30, 18);
            this.BtnSearchMT.TabIndex = 18;
            this.BtnSearchMT.UseVisualStyleBackColor = true;
            this.BtnSearchMT.Visible = false;
            this.BtnSearchMT.Click += new System.EventHandler(this.BtnSearchMT_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Manifest_Diagnostics_Tool.Properties.Resources.SideBySide;
            this.pictureBox1.Location = new System.Drawing.Point(452, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 74);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(76, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(336, 31);
            this.label1.TabIndex = 20;
            this.label1.Text = "Manifest Diagnostic Tool";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 309);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BtnSearchMT);
            this.Controls.Add(this.lblMtNotFound);
            this.Controls.Add(this.TxtWindowsSDKFolder);
            this.Controls.Add(this.BtnDisplay);
            this.Controls.Add(this.chkUseExisting);
            this.Controls.Add(this.BtnAddPath);
            this.Controls.Add(this.lstInstallPaths);
            this.Controls.Add(this.BtnDeletePath);
            this.Name = "MainWindow";
            this.Text = "Manifest Diagnostic Tool";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkUseExisting;
        private System.Windows.Forms.Button BtnAddPath;
        private System.Windows.Forms.Button BtnDeletePath;
        public System.Windows.Forms.ListBox lstInstallPaths;
        private System.Windows.Forms.Button BtnDisplay;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public System.Windows.Forms.TextBox TxtWindowsSDKFolder;
        private System.Windows.Forms.Label lblMtNotFound;
        private System.Windows.Forms.Button BtnSearchMT;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
    }
}