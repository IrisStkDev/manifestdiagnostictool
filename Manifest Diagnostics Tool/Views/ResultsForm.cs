﻿using Manifest_Diagnostics_Tool.Models;
using System;
using System.Windows.Forms;

namespace Manifest_Diagnostics_Tool.Views
{
    public partial class ResultsForm : Form
    {
        private readonly SideBySideController _controller;

        public ResultsForm(SideBySideController controller)
        {
            InitializeComponent();

            _controller = controller;

            dataGridView1.DataSource = controller.Libraries.GetFiles();
            dataGridView2.DataSource = controller.Executables.GetFiles();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOutput_Click(object sender, EventArgs e)
        {
            _controller.SaveResultsToXML();
        }
    }
}
