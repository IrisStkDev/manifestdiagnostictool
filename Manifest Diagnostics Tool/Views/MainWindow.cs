﻿using Manifest_Diagnostics_Tool.Controllers;
using Manifest_Diagnostics_Tool.Models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Manifest_Diagnostics_Tool.Views
{
    public partial class MainWindow : Form
    {
        public SideBySideController controller;

        private readonly IMTController _mTController;

        public MainWindow(IMTController mTController)
        {
            InitializeComponent();
            _mTController = mTController;
        }

        public void AddListener(SideBySideController SBSController)
        {
            controller = SBSController;
        }

        private async void BtnDisplay_Click(object sender, EventArgs e)
        {
            controller.SetExtractNewManifests(chkUseExisting.CheckState == CheckState.Checked);

            if (await controller.GetLibrariesAsync())
            {
                ResultsForm results = new ResultsForm(controller);
                results.ShowDialog();
            }
        }

        private void BtnSearchMT_Click(object sender, EventArgs e)
        {
            controller.SetNewSDKPath();
        }

        private void BtnAddPath_Click(object sender, EventArgs e)
        {
            controller.AddNewFilePath();
        }

        private void BtnDeletePath_Click(object sender, EventArgs e)
        {
            controller.DeleteSelectedListItem();
        }

        private void TxtWindowsSDKFolder_TextChanged(object sender, EventArgs e)
        {
            lblMtNotFound.Visible = !_mTController.CheckForMTInstalled(TxtWindowsSDKFolder.Text);
        }

        public void SetFormRunning(bool wait)
        {
            // TODO: some visual elements to show form is working
            BtnDisplay.Enabled = !wait;
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
