﻿using System;
using Manifest_Diagnostics_Tool.Models;
using System.Windows.Forms;

namespace Manifest_Diagnostics_Tool.Views
{
    public partial class InvisibleForm : Form
    {
        public InvisibleForm()
        {
            InitializeComponent();
        }

        private void InvisibleForm_Load(object sender, EventArgs e)
        {
            var mTController = new MTController();
            Form view = new MainWindow(mTController);
            XMLSettingsFile xmlSettingsFile = new XMLSettingsFile();
            ManifestDiagnosticAPI api = new ManifestDiagnosticAPI(xmlSettingsFile, new FileDetailList(mTController), new FileDetailList(mTController), mTController);

            SideBySideController SBSController = new SideBySideController(view, xmlSettingsFile, api);
        }
    }
}