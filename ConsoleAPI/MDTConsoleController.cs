﻿using Manifest_Diagnostics_Tool;
using Manifest_Diagnostics_Tool.Models;
using System;

namespace ConsoleAPI
{
    class MDTConsoleController
    {
        ManifestDiagnosticAPI _api;

        public MDTConsoleController(ManifestDiagnosticAPI api, string[] args)
        {
            _api = api;
        }

        public async void Run(bool interactive = false)
        {
            Console.WriteLine("Starting manifest extraction...");

            try
            {
                await _api.GetLibrariesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Fault: {ex.Message}");
                Console.Read();
                // exits with a non zero exit code, signifying an error occurred.
                Environment.Exit(2); 
            }

            foreach (FileDetail file in _api.Libraries.GetFiles())
            {
                Console.WriteLine($"manifest for {file.FileName}; file was {(file.Valid ? "Valid" : "Invalid")}");
            }

            _api.OutputToXML();
            Console.WriteLine("Extraction Complete");

            if (interactive) // interactive mode should be true if the console is ran directly
            {
                Console.WriteLine("View results? (Y/N)");
                string answer = Console.ReadLine();

                if (answer.ToLower() == "y")
                {
                    try
                    {
                        _api.OpenOutputXML();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.Read();
                    }
                }
            }

            var _exitCode = _api.Libraries.Valid && _api.Executables.Valid ? 0 : 1;
            Environment.Exit(_exitCode);
        }
    }
}
