﻿using Manifest_Diagnostics_Tool;
using Manifest_Diagnostics_Tool.Models;

namespace ConsoleAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            var mTController = new MTController();
            var api = new ManifestDiagnosticAPI(new XMLSettingsFile(), new FileDetailList(mTController), new FileDetailList(mTController), mTController);

            var console = new MDTConsoleController(api, args);
            console.Run(true);
        }
    }
}
